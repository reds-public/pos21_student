/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@soo.tech>
 * Copyright (C) 2016, 2018 Baptiste Delporte <bonel@bonel.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef SOO_H
#define SOO_H

#define AGENCY_CPU	0

#define MAX_DOMAINS	1

#ifdef __KERNEL__

/* For struct list_head */
#if !defined(__AVZ__)
#include <linux/types.h>
#include <linux/string.h>
#else
#include <list.h>
#endif

#include <asm/atomic.h>

#else /* __KERNEL__ */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef unsigned short uint16_t;

#endif /* !__KERNEL__ */

struct work_struct;
struct semaphore;

typedef uint16_t domid_t;

#ifdef __KERNEL__

void set_pfn_offset(int pfn_offset);
int get_pfn_offset(void);

#define NSECS           	1000000000ull
#define SECONDS(_s)     	((u64)((_s)  * 1000000000ull))
#define MILLISECS(_ms)  	((u64)((_ms) * 1000000ull))
#define MICROSECS(_us)  	((u64)((_us) * 1000ull))

#ifndef __ASSEMBLY__

#endif /* __ASSEMBLY__ */

int soo_hypercall(int cmd, void *vaddr, void *paddr, void *p_val1, void *p_val2);

void cache_flush_all(void);

#endif /* __KERNEL__ */

#endif /* SOO_H */
