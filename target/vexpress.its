/*
 * Copyright (C) 2020 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/dts-v1/;

/ {
	description = "Kernel and rootfs components for vExpress environment";

	images {
               
		linux {
			description = "Linux kernel";
			data = /incbin/("../linux/linux/arch/arm/boot/Image");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x81008000>;
			entry = <0x81008000>;
		};

                fdt_linux {
			description = "Linux device tree blob";
			data = /incbin/("../linux/linux/arch/arm/boot/dts/vexpress.dtb");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			load = <0x90a00000>;
		};

		initrd {
			description = "Initial rootfs (initrd)";
			data = /incbin/("../rootfs/board/vexpress/initrd.cpio");
			type = "ramdisk";
			arch = "arm";
			os = "linux";			
			compression = "none";
			load = <0x90c00000>;
		};
	};
	

        configurations {
                default = "conf_linux";

		conf_linux {
                        description = "Linux on vExpress";

                        kernel = "linux";
                        fdt = "fdt_linux";
                        ramdisk = "initrd";
		};
	};

};
